//AJAX - це технологія яка дозволяє відправляти та отримувати дані з сервер без перезавантаження веб-сторінки. Корисний при розробці Javascript, тим що зменшується кількість запитів до сервера, а також об’єм підвантажуваних даних.

function getMovies(){
    let request = fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(data => data)
        return request;
}

function getCharacters(){
    let movies = getMovies();

    movies.then(data => data.forEach(movie => {
        showMovieInfo(movie)
        movie.characters.forEach(character => {
            fetch(character)
            .then(response => response.json())
            .then(data => {
                let chars = document.querySelector(`.chars-${movie.episodeId}`);
                chars.innerHTML += `<div>${data.name}</div>`;           
            })
        })
    })) 
}

function showMovieInfo(movie){
    let movieInfo = document.querySelector('.movies');

    movieInfo.insertAdjacentHTML('beforeend', `<div>
    <h3 style="text-transform: uppercase">${movie.episodeId}. ${movie.name}</h3>
    <p>${movie.openingCrawl}</p>
    <div class="chars-${movie.episodeId}"></div>
    </div>`);
}

getCharacters();


